import 'ee/trials/track_trial_user_errors';
import { initFlashAlert } from 'ee/trials/init_flash_alert';
import { initNamespaceSelector } from 'ee/trials/init_namespace_selector';

initFlashAlert();
initNamespaceSelector();
